class Country{
    constructor(name) {
        this.name = name;
        this.cities = []
    }

    addCity(city) {
        this.cities.push(city)
    }

    deleteCity(cityName) {
        const indexToDelete = this.cities.findIndex(it => it.name === cityName)
        this.cities.splice(indexToDelete, 1)
    }
}

module.exports = {Country}