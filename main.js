const {City} = require('./City')
const {Capital} = require('./Capital')
const {Country} = require('./Country')

let kharkiv = new City('Kharkiv')
let lviv = new City('Lviv')

kharkiv.setWeather().then(() => {console.log(kharkiv)}).catch((error) => {console.log(error)})
lviv.setWeather().then(() => {console.log(lviv)}).catch((error) => {console.log(error)})
lviv.setForecast().then(() => {console.log(lviv)}).catch((error) => {console.log(error)})

let kyiv = new Capital('Kyiv')
kyiv.setAirport('Boryspil')
kyiv.setWeather().then(() => {console.log(kyiv)}).catch((error) => {console.log(error)})


let ukraine = new Country('Ukraine')

ukraine.addCity(lviv)
ukraine.addCity(kyiv)
ukraine.addCity(kharkiv)

console.log(ukraine.cities)

ukraine.cities = ukraine.cities.sort((a, b) => b.weather.temperature - a.weather.temperature)

console.log(ukraine.cities)